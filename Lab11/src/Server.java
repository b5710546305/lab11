import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;
/**
 * My own custom server for 1-1 chat
 * @author Parinvut Rochanavedya
 * @version 07-04-2015
 */
public class Server extends AbstractServer{
	/**Port number*/
	private static final int PORT = 1234;
	
	/**List of clients*/
	private List<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();
	
	/**Logged in state number*/
	static final int LOGGED_IN = 2;
	static final int LOGGED_OUT = 0;
	
	/**
	 * Constructor
	 */
	public Server(int port) {
		super(port);
	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		String message;
		if(!(msg instanceof String)){
			try {
				client.sendToClient("<<Unregconized message format");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		
		message = (String)msg;
		int state = findLoggedInClient(client);
		
		switch (state){
		case LOGGED_OUT:
			if(message.matches("Login \\w+")){
				String username = message.substring(6).trim();
				client.setInfo("username", username);
				this.sendToAllClients("Hello! "+username);
				client.setInfo("state", LOGGED_IN);
				this.clientConnected(client);
			} else {
				this.sendToClient(client,"please login");
			}
			break;
		case LOGGED_IN:
			if(message.equalsIgnoreCase("logout")){
				client.setInfo("state", LOGGED_OUT);
				this.clientDisconnected(client);
				try{
					client.sendToClient("Goodbye");
				} catch (IOException e){
					e.printStackTrace();
				}
			}
			else if(message.equals("Do Task 1")){
				try {
					client.sendToClient("What is this?");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				String username = (String)client.getInfo("username");
				System.out.println("> "+username+": "+msg);
			}
		}

	}
	
	/**
	 * Send message to selected client
	 */
	protected void sendToClient(ConnectionToClient client, String msg) {
		try {
			client.sendToClient(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Find the index of the client
	 */
	private int findLoggedInClient(ConnectionToClient client) {
		if(clients.size() <= 1){
			return LOGGED_OUT; //prevent auto login at start bug
		}
		for(int i = 0; i < clients.size(); i++){
			if(clients.get(i).equals(client)){
				return LOGGED_IN; //the client is exist in the logged in list
			}
		}
		return LOGGED_OUT;
	}
	
	/**
	 * Add a connection and (connect)
	 */
	protected void clientConnected(ConnectionToClient connection){
		clients.add(connection);
	}
	
	/**
	 * Remove a connection (disconnect)
	 */
	protected synchronized void clientDisconnected(ConnectionToClient connection){
		clients.remove(connection);
	}
	
	
	/**
	 * Appliaction console
	 */
	public static void main(String[] arg){
		Server server = new Server(PORT);
		Scanner scanner = new Scanner(System.in);
		String server_msg = null;
		try{
			server.listen();
			System.out.println("Listening on port "+PORT);
		} catch(IOException e){
			System.err.println("Server cannot be started.");
		}
		while(true){
			server_msg = scanner.nextLine();
			server.sendToAllClients("Server: "+server_msg);
		}
	}
	
}
