import java.io.IOException;
import java.net.ConnectException;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;
/**
 * A connection class thats connects to teacher's computer server
 * @author Parinvut Rochanavedya
 * @version 07-04-2015
 */
public class Connection extends AbstractClient{
	/**
	 * Constructor
	 */
	public Connection(String host, int port) {
		super(host, port);
	}

	@Override
	protected void handleMessageFromServer(Object msg) {
		if(!(msg instanceof String)){
			try {
				this.sendToServer(">>Unregconized message format");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		System.out.println("> "+msg);
		
	}
	
	/**
	 * Appliaction console
	 */
	public static void main(String[] args) throws IOException{
		Connection client = new Connection("158.108.133.19",1234);
		try{
			client.openConnection();
			String input = null;
			Scanner scanner = new Scanner(System.in);
			System.out.println("Connected to server "+client.getHost());
			while(true){
				input = scanner.nextLine();
				if(input.equalsIgnoreCase("quit")){
					client.closeConnection();
					System.out.println("Disconnected from server "+client.getHost());
					System.exit(0);
				}
				client.sendToServer(input);
			}
		} catch(ConnectException e) {
			System.err.println("Cannot connect to server "+client.getHost());
		}
	}
	
}
